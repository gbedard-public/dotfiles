# ChezMoi

Install: https://www.chezmoi.io/install/
Install: curl -sS https://starship.rs/install.sh | sh

## SSH key
Add ssh key in gitlab
* https://gitlab.com/-/user_settings/ssh_keys

## GPG key

### Create key
https://docs.gitlab.com/ee/user/project/repository/signed_commits/gpg.html#create-a-gpg-key

### Gitlab
https://docs.gitlab.com/ee/user/project/repository/signed_commits/gpg.html#add-a-gpg-key-to-your-account


## Install chezmoi
* Initialization will ask for email and git commit gpg key.
  * Find the key id with `gpg --list-secret-keys --keyid-format LONG <email>`. It looks like `39BD6D9C32EC28DF`
* Run install/init script `sh -c "$(curl -fsLS get.chezmoi.io)" -- init --purge-binary --apply git@gitlab.com:gbedard-public/dotfiles.git`


## Linux setup

### Install

```sh
# install brew
# https://brew.sh/
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# install tmux
sudo apt install tmux

# install kitty
## https://sw.kovidgoyal.net/kitty/binary/
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
sed -i "s|Icon=kitty|Icon=$(readlink -f ~)/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
sed -i "s|Exec=kitty|Exec=$(readlink -f ~)/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
echo 'kitty.desktop' > ~/.config/xdg-terminals.list

# install neovim
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz && \
sudo rm -rf /opt/nvim_linux64 && \
sudo tar -C /opt -xzf nvim-linux64.tar.gz && \
rm nvim-linux64.tar.gz

```

## MACOS Setup

### TBD

These are action to be done but not yet documented

### Install

```sh
# install homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# install kitty
## https://sw.kovidgoyal.net/kitty/binary/
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/

# install iterm2
brew install --cask iterm2

# install applications
brew install git chezmoi neovim tmux ripgrep
```

### Setup

```sh
# Init and load config from chezmoi
chezmoi init git@gitlab.com:gbedard-public/dotfiles.git # This requires an ssh key to be set in gitlab. Otherwise use https://
chezmoi apply
```

Close and reopen `iterm2`.

```sh
# if Powerlevel10k didn't offer to install fonts, run the configure manually
p10k configure
```

Close and reopen `iterm2`.


## References
https://github.com/m0lson84/dotfiles
https://github.com/alexymantha/dotfiles
https://github.com/dylanirlbeck/dotfiles
