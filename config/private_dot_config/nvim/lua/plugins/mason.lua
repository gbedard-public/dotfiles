return {
  {
    "williamboman/mason.nvim",
    opts = { ensure_installed = { "tflint", "helm-ls" } },
  }
}
