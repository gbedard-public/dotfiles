#                             _      _   _
#                            | |    | | (_)
#    ___ ___  _ __ ___  _ __ | | ___| |_ _  ___  _ __  ___
#   / __/ _ \| '_ ` _ \| '_ \| |/ _ \ __| |/ _ \| '_ \/ __|
#  | (_| (_) | | | | | | |_) | |  __/ |_| | (_) | | | \__ \
#   \___\___/|_| |_| |_| .__/|_|\___|\__|_|\___/|_| |_|___/
#                      | |
#                      |_|

zstyle ':autocomplete:*' default-context ''
zstyle ':autocomplete:*' min-delay 0.25
zstyle ':autocomplete:*' min-input 3
zstyle ':autocomplete:*' list-lines 5
zstyle ':autocomplete:history-incremental-search-*:*' list-lines 10
zstyle ':autocomplete:*' fzf-completion yes
zstyle ':autocomplete:*' widget-style menu-complete
zstyle ':completion:*:*:git*:*' ignored-patterns '*ORIG_HEAD' # To make autocomplete on "origin" better and not offer "oRIG"

# Kubernetes
if command -v kubectl >/dev/null; then
  source <(kubectl completion zsh)
fi

[[ ! -s $NVM_DIR/bash_completion ]] || \. "$NVM_DIR/bash_completion"
