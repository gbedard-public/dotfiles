# ========================================
# General
# ========================================

[core]
  autocrlf = input
  pager = less --mouse
	excludesfile = {{ .chezmoi.homeDir }}/.config/git/ignore


# ========================================
# User
# ========================================

[user]
  name = Gabriel Bedard
  email = {{ .email | quote }}
  signingkey = {{ .signingKey | quote }}

# ========================================
# Operations
# ========================================

[checkout]
  defaultRemote = origin

[commit]
  gpgsign = true
  template = {{ .chezmoi.homeDir }}/.config/git/message.txt

[push]
  autoSetupRemote = true

[pull]
  rebase = true

# ========================================
# Format
# ========================================

[color]
ui = auto

[color "branch"]
current = yellow reverse
local = yellow
remote = green

[color "diff"]
meta = yellow bold
frag = magenta bold
old = red bold
new = green bold

[color "status"]
added = yellow
changed = green
untracked = red

[pretty]
changelog = "%C(yellow)%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset"
